#!/usr/bin/env python
#coding=utf-8

from toughradius.console.libs import pyforms
from toughradius.console.libs.pyforms.rules import button_style, input_style

mac_auth_update_form = pyforms.Form(
    pyforms.Hidden("id", description=u"编号"),
    pyforms.Textbox("account_number", description=u"用户账号", readonly="readonly", required="required", **input_style),
    pyforms.Textbox("mac_addr", description=u"MAC地址", readonly="readonly", **input_style),
    pyforms.Textbox("end_date", description=u"Mac认证失效时间", required="required", **input_style),
    pyforms.Button("submit", type="submit", html=u"<b>更新</b>", **button_style),
    title=u"修改Mac地址认证信息",
    action="/mac/auth/update"
)
