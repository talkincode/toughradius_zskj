#!/usr/bin/env python
# coding=utf-8

import sys, os
from bottle import Bottle
from bottle import request
from bottle import response
from bottle import redirect
from bottle import MakoTemplate
from bottle import static_file
from bottle import abort
from beaker.cache import cache_managers
from toughradius.console.libs.paginator import Paginator
from toughradius.console.libs import utils
from toughradius.console.websock import websock
from toughradius.console import models
from toughradius.console.base import *
from toughradius.console.admin import mac_auth_forms
from hashlib import md5
from twisted.python import log
import bottle
import datetime
import json
import functools

__prefix__ = "/mac/auth"

app = Bottle()
app.config['__prefix__'] = __prefix__

###############################################################################
# roster manage
###############################################################################

@app.route('/', apply=auth_opr, method=['GET', 'POST'])
def mac_auth(db, render):
    _query = db.query(models.SlcAccountMacAuth)
    return render("sys_mac_auth_list",
                  page_data=get_page_data(_query))


@app.post('/', apply=auth_opr)
def mac_auth_post(db, render):
    account_number = request.params.get('account_number')
    mac_addr = request.params.get('mac_addr')
    print account_number
    _query = db.query(models.SlcAccountMacAuth)

    if account_number:
        _query = _query.filter(models.SlcAccountMacAuth.account_number.like('%' + account_number + '%'))

    if mac_addr:
        _query = _query.filter(models.SlcAccountMacAuth.mac_addr == mac_addr)

    return render("sys_mac_auth_list", page_data=get_page_data(_query), **request.params)


@app.get('/update', apply=auth_opr)
def mac_auth_update(db, render):
    auth_id = request.params.get("auth_id")
    form = mac_auth_forms.mac_auth_update_form()
    form.fill(db.query(models.SlcAccountMacAuth).get(auth_id))
    return render("sys_mac_auth_form", form=form)


@app.post('/update', apply=auth_opr)
def mac_auth_update_post(db, render):
    form = mac_auth_forms.mac_auth_update_form()
    if not form.validates(source=request.forms):
        return render("sys_roster_form", form=form)
    mac_auth = db.query(models.SlcAccountMacAuth).get(form.d.id)
    mac_addr = mac_auth.mac_addr
    mac_auth.end_date = form.d.end_date

    ops_log = models.SlcRadOperateLog()
    ops_log.operator_name = get_cookie("username")
    ops_log.operate_ip = get_cookie("login_ip")
    ops_log.operate_time = utils.get_currtime()
    ops_log.operate_desc = u'操作员(%s)修改(%s)Mac地址认证信息:%s' % (
        get_cookie("username"), mac_auth.account_number, mac_auth.mac_addr)
    db.add(ops_log)

    db.commit()
    websock.update_cache("mac_auth", mac_addr=mac_addr)
    redirect("/mac/auth")


@app.get('/delete', apply=auth_opr)
def mac_auth_delete(db, render):
    auth_id = request.params.get("auth_id")
    mac_auth = db.query(models.SlcAccountMacAuth).get(auth_id)
    mac_addr = mac_auth.mac_addr
    db.query(models.SlcAccountMacAuth).filter_by(id=auth_id).delete()

    ops_log = models.SlcRadOperateLog()
    ops_log.operator_name = get_cookie("username")
    ops_log.operate_ip = get_cookie("login_ip")
    ops_log.operate_time = utils.get_currtime()
    ops_log.operate_desc = u'操作员(%s)删除Mac地址认证信息:%s' % (get_cookie("username"), mac_auth.account_number or '')
    db.add(ops_log)

    db.commit()
    websock.update_cache("mac_auth", mac_addr=mac_addr)
    redirect("/mac/auth")


@app.get('/deletes', apply=auth_opr)
def mac_auth_deletes(db, render):
    auth_ids = list(eval(request.params.get("auth_ids")))
    for auth_id in auth_ids:
        mac_auth = db.query(models.SlcAccountMacAuth).get(auth_id)
        mac_addr = mac_auth.mac_addr
        db.query(models.SlcAccountMacAuth).filter_by(id=auth_id).delete()
        websock.update_cache("mac_auth", mac_addr=mac_addr)

    ops_log = models.SlcRadOperateLog()
    ops_log.operator_name = get_cookie("username")
    ops_log.operate_ip = get_cookie("login_ip")
    ops_log.operate_time = utils.get_currtime()
    ops_log.operate_desc = u'操作员(%s)批量删除Mac地址认证信息' % (get_cookie("username"))
    db.add(ops_log)

    db.commit()
    redirect("/mac/auth")


permit.add_route(r"/mac/auth", u"Mac地址认证", u"系统管理", is_menu=True, order=7.001)
permit.add_route(r"/mac/auth/update", u"修改Mac地址认证信息", u"系统管理", order=7.002)
permit.add_route(r"/mac/auth/delete", u"删除Mac地址认证信息", u"系统管理", order=7.003)
permit.add_route(r"/mac/auth/deletes", u"批量删除Mac地址认证信息", u"系统管理", order=7.04)
